
function Shuffle(o) {
    for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
    };



//I was unable to get the cards to reset after they are initally flipped. I had a hard time figuring out how to re select the dive from case 0 to update the class value and add the _ back to the beginning of the file.

$(function () {
    var clickCount;
    var matches, $matches;
    var cardClasses = ["_ace", "_king", "_queen", "_jack", "_ace", "_king", "_queen", "_jack"];
    var $cards;
    var $resetBtn;
    var pair;
    var storeClass1,storeClass2;
    var storedClasses = [];
    var storeDiv1,storeDiv2;
    
    
    
    $cards = $('#cards div'); 
    $matches = $('#matches');
    $resetBtn = $('#reset');
    
    
    
    
    function init() {
        clickCount = 0;
        matches = Number($matches.text());
        pair = [];
        Shuffle(cardClasses);
        
        for (let i=0; i<$cards.length; i++) {
            var $div = $cards.get(i);
            $div.setAttribute('class', cardClasses[i]);
            
         }   
        
    }
    
    

    function checkForMatch() {
        let classVal = $(this).attr('class');
        
        function updateClass(div) {
            if (classVal[0] === '_') { 
                classVal = classVal.slice(1);
                div.attr('class', classVal);
                pair.push(classVal);
                clickCount++;
            }
        }
        
        
        switch(clickCount) {
            case 0:
                console.log($(this));
                storeDiv1 = $(this).index();
                storeClass1 = $(this).attr('class');
                updateClass($(this));
                break;
                
            case 1:
                storeClass2 = $(this).attr('class');
                console.log(storeClass2);
                updateClass($(this));
                break;
                
            case 2:
                if (pair[0] === pair[1]) {
                    alert("Its a match!");
                    matches++;
                    console.log(matches);
                    $matches.text(matches);
                    clickCount = 0;
                    pair = [];
                    
                } 
                
                if (pair[0] != pair[1]) {
                    alert("Not a match");
                    clickCount = 0;
                    pair = [];
                    
                }
                    
                
                break;
            default:
                console.log("oh no");
                
        } 
          
   }
    
    function resetGame() {
        //remove all class attributes
        $cards.each(function () {
            $(this).removeClass();
            $matches.text(0);
            pair = [];
        });
        
        //re-initialize
        init();
            
        
    }
    
    
    $cards.on('click', checkForMatch);
    $resetBtn.on('click', resetGame);
    init();
   
});