
var fullName = document.getElementById('fullname');
var email = document.getElementById('email');
var form = document.getElementById('contactForm');
var checkBox = document.getElementById('consent');
var errors = document.getElementById('errors');

fullName.addEventListener('focus', function() {
    if (this.value == ''){
        this.value = 'Please type your full name...';
    
    }
});


form.addEventListener('submit', function(e){
    var stopSubmit = false;
    
    if (!checkBox.checked) {
        errors.innerHTML = '<p>You must consent to reciveing emails!</p>';
        stopSubmit = true;
    }
    
    if (fullName.value == '') {
        errors.innerHTML += '<p>You must enter your name!</p>';
        stopSubmit = true;
    }
    
    if (stopSubmit) {
        e.preventDefault();
    }
});
